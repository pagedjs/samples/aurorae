A book sample made with pagedjs

## To-do list
- [ ] appendix layout + breaks
- [ ] appendis stule
- [ ] table of figure
- [ ] styles of tables
- [ ] index
- [ ] plates and figures

## Need to implement in pagedjs
- [x] breaks
- [ ] `break-after: avoid;`, `break-before: avoid`
- [ ] Computing Page-margin Box Dimension (#54)
- [x] Selecting each first page of a page group (#28)
    - [ ] `content: none;` for margin boxes
- [ ] Ordered list restart numbering on page break (#26)
- [ ] Text alignment in margin box
- [ ] breaks and absolute positionning
